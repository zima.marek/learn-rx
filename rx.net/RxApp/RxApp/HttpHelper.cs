﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reactive.Linq;
using System.Reactive.Threading.Tasks;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace RxApp
{
    public class HttpHelper
    {
        private const string BaseAddress = "https://api.stackexchange.com/2.2/";
        private static readonly HttpClient HttpClient = new HttpClient()
        {
            BaseAddress = new Uri(BaseAddress)
        };

        public static IObservable<T> Get<T>(string url)
        {
            return HttpGet<T>(url).ToObservable();
        }

        private static async Task<string> HttpGet(string url)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url)))
            {
                request.Headers.TryAddWithoutValidation("Accept", "application/json");

                using (var response = await HttpClient.SendAsync(request).ConfigureAwait(false))
                {
                    response.EnsureSuccessStatusCode();
                    using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                    using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                    using (var streamReader = new StreamReader(decompressedStream))
                    {
                        var responseString = await streamReader.ReadToEndAsync().ConfigureAwait(false);
                        return responseString;
                    }
                }
            }
        }

        private static async Task<T> HttpGet<T>(string url)
        {
            return JsonConvert.DeserializeObject<T>(await HttpGet(url));
        }
    }
}
