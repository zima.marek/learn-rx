﻿using NUnit.Framework;
using RxApp.Koans.Utils;
using CurrentLesson = RxApp.Koans.Lessons.Lesson5Events;

namespace RxApp.Koans.Tests
{
	[TestFixture]
	public class Lesson5EventsTest
	{
		[Test]
		public void TestAllQuestions()
		{
			KoanUtils.Verify<CurrentLesson>(l => l.TheMainEvent(), "BAR");
		}
	}
}