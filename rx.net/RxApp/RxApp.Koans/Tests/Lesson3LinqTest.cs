﻿using System.Windows;
using NUnit.Framework;
using RxApp.Koans.Utils;
using CurrentLesson = RxApp.Koans.Lessons.Lesson3Linq;

namespace RxApp.Koans.Tests
{
	[TestFixture]
	public class Lesson3LinqTest
	{
		[Test]
		public void TestAllQuestions()
		{
			KoanUtils.Verify<CurrentLesson>(l => l.Linq(), 11);
			KoanUtils.Verify<CurrentLesson>(l => l.LinqOverMouseEvents(), new Point(75, 75));
		}
	}
}