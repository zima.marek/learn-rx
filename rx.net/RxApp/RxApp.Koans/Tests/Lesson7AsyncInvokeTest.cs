﻿using System;
using NUnit.Framework;
using RxApp.Koans.Utils;
using CurrentLesson = RxApp.Koans.Lessons.Lesson7AsyncInvoke;

namespace RxApp.Koans.Tests
{
	[TestFixture]
	public class Lesson7AsyncInvokeTest
	{
		[Test]
		public void TestAllQuestions()
		{
			Action<CurrentLesson> action = l =>{l.____ = "C";l._____ = "A";l.______ = "B";};
			KoanUtils.AssertLesson<CurrentLesson>(l => l.TheBloodyHardAsyncInvokationPatter(), action);
			KoanUtils.Verify<CurrentLesson>(l => l.NiceAndEasyFromAsyncPattern(), 48);
			// Note: I don't know how to properly test AsynchronousRuntimeIsNotSummed
			KoanUtils.Verify<CurrentLesson>(l => l.TimeoutMeansStopListeningDoesNotMeanAbort(), "Give me 5, Joe");
			KoanUtils.Verify<CurrentLesson>(l => l.AsynchronousObjectsAreProperlyDisposed(), "D");
		}
		
	}
}