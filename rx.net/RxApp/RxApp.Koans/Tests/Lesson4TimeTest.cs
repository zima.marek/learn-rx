﻿using NUnit.Framework;
using RxApp.Koans.Utils;
using CurrentLesson = RxApp.Koans.Lessons.Lesson4Time;

namespace RxApp.Koans.Tests
{
	[TestFixture]
	public class Lesson4TimeTest
	{
		[Test]
		
		public void TestAllQuestions()
		{
			KoanUtils.Verify<CurrentLesson>(l => l.LaunchingAnActionInTheFuture(), 0);
			KoanUtils.Verify<CurrentLesson>(l => l.LaunchingAnEventInTheFuture(), 0);
			KoanUtils.Verify<CurrentLesson>(l => l.YouCanPlaceATimelimitOnHowLongAnEventShouldTake(), 2100);
			KoanUtils.Verify<CurrentLesson>(l => l.Throttling(), "from scott");
			KoanUtils.Verify<CurrentLesson>(l => l.Buffering(), "Scott Reed");
			KoanUtils.Verify<CurrentLesson>(l => l.TimeBetweenCalls(), "slow down");

		}


	}
}