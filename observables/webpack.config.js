var path = require('path');

module.exports = {
    entry: "./index.ts",
    mode: "development",
    output: {
        path: path.resolve(__dirname, "app/build"),
        filename: "bundle.js"
    },
    devtool: "source-map",
    node: {
        fs: "empty",
        module: "empty"
    },
    stats: {
        warnings: false
    },
    performance: {
        hints: false
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    module: {
        rules: [
            // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
            { test: /\.tsx?$/, loader: "ts-loader" }
        ]
    }
}