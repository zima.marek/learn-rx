import { Observable, fromEvent, pipe, of, interval, empty, from, range, timer, Subject, combineLatest } from 'rxjs';
import { filter, take, publish, share, single, takeWhile, distinctUntilChanged, debounceTime, map, switchMap } from 'rxjs/operators';
import { httpGetAsync } from './functions';

// js history


// ugly callbacks
/*httpGetAsync('https://api.stackexchange.com/2.2/tags?order=desc&sort=popular&site=stackoverflow', x => {
    console.log(x);
});*/

// ugly promises
/*fetch('https://api.stackexchange.com/2.2/tags?order=desc&sort=popular&site=stackoverflow')
    .then(x => x.json())
    .then(x => console.log(x));*/

// nice rxjs
/*const promise = fetch('https://api.stackexchange.com/2.2/tags?order=desc&sort=popular&site=stackoverflow');
const promise$ = from(promise);

promise$.pipe(
    switchMap((x: any) => from(x.json()))
).subscribe((x: any) => console.log(x));*/





// DOM rx

document.write('<input type="text" id="text1"/><button id="button1">Click!</button><p id="insert1"></p>');


// EVENTS
const text1 = document.getElementById('text1');
const btn1 = document.getElementById('button1');
const insert1 = document.getElementById('insert1');

const btn1Click = fromEvent(btn1, 'click');
const text1Changed = fromEvent(text1, 'keyup');
const insert1Changed = fromEvent(insert1, 'change');

//btn1Click.subscribe(x => alert('click'));


// TODO - filter words, map result, debounce time, distinct until change
text1Changed.pipe(
    filter((x: any) => x.target.value.length > 3 || x.target.value.indexOf(' ') > -1)
).subscribe(x => {
    const target = x.target as HTMLInputElement;
    insert1.innerText = target.value;
});




// cold

/*let obs = Observable.create(observer => observer.next(Date.now()));
obs.subscribe(v => console.log("1st subscriber: " + v));
obs.subscribe(v => console.log("2nd subscriber: " + v));*/

// warmer

/*let obs2 = Observable
    .create(observer => observer.next(Date.now()))
    .pipe(publish());

obs2.subscribe(v => console.log("1st subscriber: " + v));
obs2.subscribe(v => console.log("2nd subscriber: " + v));

obs2.connect();*/

// hot

let obs3 = Observable
    .create(observer => observer.next(Date.now()))
    .pipe(publish())
obs3.connect();

obs3.subscribe(v => console.log("1st subscriber: " + v));
obs3.subscribe(v => console.log("2nd subscriber: " + v));







// operators
// creation
const emptySource = empty();

const arraySource = from([1, 2, 3, 4, 5]);

const intervalSource = interval(1000);

const ofSource = of({ name: 'Brian' }, [1, 2, 3], function hello() {
    return 'Hello';
});

const sourceRange = range(1, 10);

const sourceTimer = timer(1000, 2000);

// filtering - skip, take, skipWhile, takeWhile, takeUntil, skipUntil, filter, first, single, take, last
const sourceFromSingle = from([1, 2, 3, 4, 5]);
const sourceSingle = sourceFromSingle.pipe(single(val => val === 4));

const example = sourceFromSingle.pipe(takeWhile(val => val <= 4));


// subject

/*var subj = new Subject<number>();
subj.next(5);
subj.subscribe(x => console.log(x));*/

// behavior subject TODO



